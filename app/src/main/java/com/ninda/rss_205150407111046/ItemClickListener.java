package com.ninda.rss_205150407111046;

import android.view.View;

public interface ItemClickListener {

    void onClick(View view, int position, boolean isLongClick);

}
