package com.ninda.rss_205150407111046.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ninda.rss_205150407111046.ItemClickListener;
import com.ninda.rss_205150407111046.R;
import com.ninda.rss_205150407111046.model.Item;
import com.ninda.rss_205150407111046.model.RSSObject;

class FeedViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener
{
    public TextView tvTitle, tvAuthor, tvDate, tvDesc;
    private ItemClickListener itemClickListener;

    public FeedViewHolder(View itemView) {
        super(itemView);
        tvTitle = (TextView) itemView.findViewById(R.id.tv_item_title);
        tvAuthor = (TextView) itemView.findViewById(R.id.tv_item_author);
        tvDate = (TextView) itemView.findViewById(R.id.tv_item_date);
        tvDesc = (TextView) itemView.findViewById(R.id.tv_item_desc);

        itemView.setOnClickListener(this);
        itemView.setOnLongClickListener(this);
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View view) {
        itemClickListener.onClick(view, getAdapterPosition(), false);
    }

    @Override
    public boolean onLongClick(View view) {
        itemClickListener.onClick(view, getAdapterPosition(), true);
        return true;
    }
}

public class FeedAdapter extends RecyclerView.Adapter<FeedViewHolder>{

    private RSSObject rssObject;
    private Context context;
    private LayoutInflater layoutInflater;

    public FeedAdapter(RSSObject rssObject, Context context) {
        this.rssObject = rssObject;
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public FeedViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = layoutInflater.inflate(R.layout.item_row_rss, parent, false);
        return new FeedViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull FeedViewHolder holder, int position) {
        String desc = rssObject.getItems().get(position).getDescription().replaceAll("(?s)<[^>]*>(\\s*<[^>]*>)*", " ");
        desc = desc.substring(1);

        holder.tvTitle.setText(rssObject.getItems().get(position).getTitle());
        holder.tvAuthor.setText("by " + rssObject.getItems().get(position).getAuthor());
        holder.tvDate.setText(rssObject.getItems().get(position).getPubDate());
        holder.tvDesc.setText(desc);

        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position, boolean isLongClick) {
                if(!isLongClick)
                {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(rssObject.getItems().get(position).getLink()));
                    browserIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(browserIntent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return rssObject.items.size();
    }
}
